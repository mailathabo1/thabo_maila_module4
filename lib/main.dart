import 'package:flutter/material.dart';
import 'package:thabo_maila_module4/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const SplashScreen(),
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          backgroundColor: Colors.grey[300],
    ),
    );
  }
}
